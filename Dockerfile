FROM debian:buster
RUN apt-get update \
	&& apt-get install -y apache2 ruby2.5 bundler \
	&& apt-get clean
ARG INGRESS_HOME=/var/lib/ingress
RUN mkdir -p $INGRESS_HOME
WORKDIR $INGRESS_HOME
COPY Gemfile Gemfile.lock ./
RUN bundle install --deployment --no-cache --without development
COPY ingress/* ./
ENTRYPOINT ["bundle", "exec", "./ingress.rb"]
